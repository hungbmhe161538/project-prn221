﻿using FinalProjectPRN221.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FinalProjectPRN221.Pages.Products
{
    public class OrderDetailsModel : PageModel
    {
        private readonly Project_PRN221Context _context;

        public OrderDetailsModel(Project_PRN221Context context)
        {
            _context = context;
        }

        [BindProperty(SupportsGet = true)]
        public int? OrderId { get; set; }

        public List<OrderDetail> OrderItems { get; set; }

        public string Message { get; set; }

        public IActionResult OnGet()
        {
            if (HttpContext.Request.Cookies["LoginCookie"] == null)
            {
                // Chuyển hướng đến trang đăng nhập
                return RedirectToPage("/Account/Login");
            }
            else
            {
                LoadOrderDetails();
                return Page();
            }
        }

        private void LoadOrderDetails()
        {
            var groupedOrderItems = _context.OrderDetails
                .Where(oi => oi.OrderId == OrderId && oi.Order != null && oi.Order.IsPaid == "0")
                .GroupBy(oi => oi.ProductId)
                .Select(group => new OrderDetail
                {
                    ProductId = group.Key.Value,
                    Quantity = group.Sum(item => item.Quantity),
                    UnitPrice = group.First().UnitPrice,
                    Product = group.First().Product
                })
                .ToList();

            OrderItems = groupedOrderItems;
        }
        public IActionResult OnPostDeleteProduct(int productId)
        {
            var orderDetailsToDelete = _context.OrderDetails.Where(od => od.ProductId == productId).ToList();

            if (orderDetailsToDelete != null)
            {
                _context.OrderDetails.RemoveRange(orderDetailsToDelete);
                _context.SaveChanges();

                // Lấy orderId từ OrderItems (hoặc từ đâu đó)
                int? orderId = orderDetailsToDelete.FirstOrDefault()?.OrderId;

                TempData["Message"] = "Product deleted successfully!";
                // Chuyển hướng về trang OrderDetails với orderId
                return RedirectToPage("/Products/OrderDetails", new { orderId });
            }

            return Page();
        }

        public IActionResult OnPostUpdateQuantity(int productId, int newQuantity, int orderId)
        {
            var orderDetail = _context.OrderDetails
                .Include(od => od.Order)
                .FirstOrDefault(od => od.ProductId == productId && od.OrderId == orderId);

            if (orderDetail != null && orderDetail.Order != null)
            {
                // Lưu giá trị cũ của Quantity
                int? oldQuantity = orderDetail.Quantity;

                // Cập nhật Quantity mới
                orderDetail.Quantity = newQuantity;

                // Kiểm tra xem Order có tồn tại không
                if (orderDetail.Order != null)
                {
                    UpdateTotalAmount(orderId);
                }

                TempData["Message"] = "Quantity updated successfully!";
            }

            return RedirectToPage("/Products/OrderDetails", new { orderId });
        }

        public IActionResult OnPostProcessPayment(int orderId)
        {
            var order = _context.Orders
                .Include(o => o.OrderDetails)
                .FirstOrDefault(o => o.OrderId == orderId);
            if (order != null)
            {
                // Kiểm tra số lượng sản phẩm còn đủ để trừ hay không
                foreach (var orderDetail in order.OrderDetails)
                {
                    var product = _context.Products.FirstOrDefault(p => p.ProductId == orderDetail.ProductId);

                    if (product != null && product.StockQuantity >= orderDetail.Quantity)
                    {
                        // Trừ đi số lượng sản phẩm từ kho
                        product.StockQuantity -= orderDetail.Quantity;
                    }
                    else
                    {
                        // Xử lý trường hợp không đủ số lượng sản phẩm trong kho
                        // Có thể hiển thị thông báo lỗi và không thực hiện thanh toán
                        TempData["Message"] = $"Not enough quantity in stock for product: {product.ProductName}";
                        return Page();
                    }
                }

                // Đặt trạng thái thanh toán thành "Đã thanh toán"
                order.IsPaid = "1";

                // Cập nhật tổng tiền
                UpdateTotalAmount(orderId);

                TempData["Message"] = "Payment successful!";
                // Chuyển hướng về trang OrderDetails với orderId và hiển thị thông báo
                return RedirectToPage("/Products/OrderDetails", new { orderId });
            }

            return Page();
        }
        private void UpdateTotalAmount(int orderId)
        {
            var orderDetails = _context.OrderDetails.Where(od => od.OrderId == orderId).ToList();

            // Lấy Order theo orderId
            var order = _context.Orders.FirstOrDefault(o => o.OrderId == orderId);

            if (order != null)
            {
                // Cập nhật TotalAmount của Order bằng tổng của (UnitPrice * Quantity)
                order.TotalAmount = orderDetails.Sum(od => od.UnitPrice * od.Quantity);
            }

            _context.SaveChanges();
        }
    }
}