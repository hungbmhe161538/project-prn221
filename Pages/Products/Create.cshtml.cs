﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FinalProjectPRN221.Models;

namespace FinalProjectPRN221.Pages.Products
{
    public class CreateModel : PageModel
    {
        private readonly Project_PRN221Context _context;

        public CreateModel(Project_PRN221Context context)
        {
            _context = context;
        }
        public SelectList CategoryOptions { get; set; }
        public SelectList SupplierOptions { get; set; }
        public IActionResult OnGet()
        {
            if (HttpContext.Request.Cookies["LoginCookie"] == null)
            {
                // Chuyển hướng đến trang đăng nhập
                return RedirectToPage("/Account/Login");
            }
            else
            {
                var categories = _context.Categories.ToList();
                var suppliers = _context.Suppliers.ToList();

                // Tạo danh sách lựa chọn từ danh sách danh mục
                CategoryOptions = new SelectList(categories, nameof(Category.CategoryId), nameof(Category.CategoryName));
                SupplierOptions = new SelectList(suppliers, nameof(Supplier.SupplierId), nameof(Supplier.SupplierName));

                // Continue processing for the logged-in user
            }

            // You might return a page or perform additional actions here
            return Page();
        }

        [BindProperty]
        public Product Product { get; set; } = default!;
        public Category Category { get; set; } = new Category();
        public Supplier Supplier { get; set; } = new Supplier();


        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (_context.Products == null || Product == null)
            {
                return Page();
            }

            _context.Products.Add(Product);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }

        
    }
}
