﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FinalProjectPRN221.Models;
using System.Security.Claims;

namespace FinalProjectPRN221.Pages.Products
{
    public class EditModel : PageModel
    {
        private readonly Project_PRN221Context _context;

        public EditModel(Project_PRN221Context context)
        {
            _context = context;
        }

        [BindProperty]
        public Product Product { get; set; } = default!;
        public SelectList CategoryOptions { get; set; }
        public SelectList SupplierOptions { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (HttpContext.Request.Cookies["LoginCookie"] == null)
            {
                // Chuyển hướng đến trang đăng nhập
                return RedirectToPage("/Account/Login");
            }
            else
            {
                if (id == null || _context.Products == null)
                {
                    return NotFound();
                }

                var product = await _context.Products.FirstOrDefaultAsync(m => m.ProductId == id);
                if (product == null)
                {
                    return NotFound();
                }
                Product = product;
                var categories = _context.Categories.ToList();
                CategoryOptions = new SelectList(categories, nameof(Category.CategoryId), nameof(Category.CategoryName));
                var suppliers = _context.Suppliers.ToList();
                SupplierOptions = new SelectList(suppliers, nameof(Supplier.SupplierId), nameof(Supplier.SupplierName));
                return Page();
            }
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            var userRoleId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value;

            if (userRoleId == "1")
            {
                // Người dùng có quyền truy cập vào phương thức này
                _context.Attach(Product).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(Product.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToPage("./Index");
            }
            else
            {
                TempData["ErrorMessage"] = "Bạn không có quyền này";
                return RedirectToPage("/Products/Index");
            }
        }

        private bool ProductExists(int id)
        {
          return (_context.Products?.Any(e => e.ProductId == id)).GetValueOrDefault();
        }
    }
}
