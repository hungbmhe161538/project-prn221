﻿using FinalProjectPRN221.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;

namespace FinalProjectPRN221.Pages.Products
{
    public class IndexModel : PageModel
    {
        private readonly Project_PRN221Context _context;
        private readonly IConfiguration Configuration;

        public IndexModel(Project_PRN221Context context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }
        public List<Product> ListOfProduct = new List<Product>();
        public List<Category> ListOfCategory = new List<Category>();
        public List<Supplier> ListOfSuppliers = new List<Supplier>();
        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }
        public string IdSort { get; set; }
        public PaginatedList<Product> Products { get; set; }
        public async Task<IActionResult> OnGetAsync(string sortOrder, string currentFilter, string searchString, int categoryId, int supplierId, int? pageIndex)
        {
            if (HttpContext.Request.Cookies["LoginCookie"] == null)
            {
                // Chuyển hướng đến trang đăng nhập
                return RedirectToPage("/Account/Login");
            }
            else
            {
                CurrentSort = sortOrder;
                IdSort = string.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
                DateSort = sortOrder == "Date" ? "date_desc" : "Date";
                NameSort = sortOrder == "Name" ? "name_desc" : "Name";
                ListOfCategory = _context.Categories.ToList();
                ListOfSuppliers = _context.Suppliers.ToList();

                IQueryable<Product> query = _context.Products
                    .Include(x => x.Category)
                    .Include(x => x.Supplier);

                if (categoryId != 0)
                {
                    query = query.Where(p => p.CategoryId == categoryId);
                }

                if (supplierId != 0)
                {
                    query = query.Where(p => p.SupplierId == supplierId);
                }
                if (searchString != null)
                {
                    pageIndex = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                CurrentFilter = searchString;
                if (!string.IsNullOrEmpty(searchString))
                {
                    query = query.Where(s => s.ProductName.Contains(searchString));
                }
                switch (sortOrder)
                {
                    case "name_desc":
                        query = query.OrderByDescending(s => s.ProductName);
                        break;
                    case "Date":
                        query = query.OrderBy(s => s.ExpirationDate);
                        break;
                    case "date_desc":
                        query = query.OrderByDescending(s => s.ExpirationDate);
                        break;
                    case "Name": // Sorting by ID
                        query = query.OrderBy(s => s.ProductName);
                        break;
                    case "id_desc": // Sorting by ID in descending order
                        query = query.OrderByDescending(s => s.ProductId);
                        break;
                    default:
                        query = query.OrderBy(s => s.ProductId);
                        break;
                }
                ListOfProduct = await query.ToListAsync();
                if (ListOfProduct != null)
                {
                    var pageSize = Configuration.GetValue("PageSize", 5);
                    Products = await PaginatedList<Product>.CreateAsync(query.AsNoTracking(), pageIndex ?? 1, pageSize);
                }
                return Page();
            }
            
        }
        public IActionResult OnGetDelete(int productId)
        {
            var userRoleId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value;

            if (userRoleId == "1")
            {
                var product = _context.Products.Find(productId);
                if (product != null)
                {
                    var orderDetails = _context.OrderDetails
                        .Where(x => x.ProductId == product.ProductId).ToList();

                    foreach (var orderDetail in orderDetails)
                    {
                        _context.OrderDetails.Remove(orderDetail);
                    }

                    _context.Products.Remove(product);
                    _context.SaveChanges();

                    // Redirect back to the index page
                    return RedirectToPage("/Products/Index");
                }
                else
                {
                    TempData["ErrorMessage"] = "Không tìm thấy sản phẩm này!!!";
                    return RedirectToPage("/Products/Index");
                }
            }
            else
            {
                // Return a message indicating insufficient permissions
                TempData["ErrorMessage"] = "Bạn không có quyền hạn này.";
                return RedirectToPage("/Products/Index");
            }
        }
        [BindProperty]
        public int? OrderId { get; set; }

        public async Task<IActionResult> OnPostAddToOrderAsync(int productId)
        {
            var currentUserIdClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            if (currentUserIdClaim != null && int.TryParse(currentUserIdClaim.Value, out int currentUserId))
            {
                var product = await _context.Products.FindAsync(productId);

                if (product != null)
                {
                    var order = await _context.Orders
                        .Include(o => o.OrderDetails)
                        .Where(o => o.UserId == currentUserId && o.IsPaid == "0")
                        .FirstOrDefaultAsync();

                    if (order == null)
                    {
                        order = new Models.Order
                        {
                            UserId = currentUserId,
                            OrderDate = DateTime.Now,
                            OrderDetails = new List<OrderDetail>()
                        };

                        _context.Orders.Add(order);

                        await _context.SaveChangesAsync();

                        OrderId = order.OrderId;
                    }
                    else
                    {
                        OrderId = order.OrderId;
                    }

                    var orderItem = new OrderDetail
                    {
                        ProductId = product.ProductId,
                        Quantity = 1,
                        UnitPrice = product.Price ?? 0,
                    };

                    order.OrderDetails.Add(orderItem);

                    await _context.SaveChangesAsync();
                }

                return RedirectToPage("/Products/OrderDetails", new { orderId = OrderId });
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
    }
}
