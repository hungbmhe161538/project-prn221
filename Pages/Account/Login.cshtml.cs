﻿using FinalProjectPRN221.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;

namespace FinalProjectPRN221.Pages.Account
{
    public class LoginModel : PageModel
    {
        private readonly Project_PRN221Context _context;

        [BindProperty]
        public string UserName { get; set; }

        [BindProperty]
        public string Password { get; set; }

        public LoginModel(Project_PRN221Context context)
        {
            _context = context;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = _context.Users.FirstOrDefault(u => u.Username == UserName && u.Password == Password);

            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()), // Lưu ID của người dùng
                    new Claim(ClaimTypes.Name, user.Username), // Lưu tên người dùng
                    new Claim(ClaimTypes.Role, user.RoleId.ToString()) // Lưu vai trò của người dùng
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var authProperties = new AuthenticationProperties
                {
                    IsPersistent = true, // True nếu bạn muốn cookie được lưu trữ vĩnh viễn
                    ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(30) // Thời gian tồn tại của cookie
                };

                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);

                return RedirectToPage("/Products/Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return Page();
            }
        }
    }
}

