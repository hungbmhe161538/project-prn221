using FinalProjectPRN221.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FinalProjectPRN221.Pages.Account
{
    public class RegisterModel : PageModel
    {
        private readonly Project_PRN221Context _context;

        [BindProperty]
        public User Input { get; set; }

        public RegisterModel(Project_PRN221Context context)
        {
            _context = context;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            Input.RoleId = 2;
            if (ModelState.IsValid)
            {
                _context.Users.Add(Input);
                await _context.SaveChangesAsync();

                // Redirect to a success page or perform additional actions
                return RedirectToPage("./Login");
            }

            return Page();
        }
    }
}
