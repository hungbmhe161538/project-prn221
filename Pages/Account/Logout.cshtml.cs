﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FinalProjectPRN221.Pages.Account
{
    public class LogoutModel : PageModel
    {
        public IActionResult OnPost()
        {
            // Hủy cookie đăng nhập
            Response.Cookies.Delete("LoginCookie");

            // Chuyển hướng về trang chủ hoặc trang đăng nhập (tuỳ chọn)
            return RedirectToPage("");
        }
    }
}
