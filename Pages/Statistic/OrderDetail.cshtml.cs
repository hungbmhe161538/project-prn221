﻿using FinalProjectPRN221.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace FinalProjectPRN221.Pages.Statistic
{
    public class OrderDetailModel : PageModel
    {
        private readonly Project_PRN221Context _context;

        public OrderDetailModel(Project_PRN221Context context)
        {
            _context = context;
        }

        public Order Order { get; set; }
        public IQueryable<OrderDetail> OrderDetails { get; set; }

        public IActionResult OnGet(int? id)
        {
            if (HttpContext.Request.Cookies["LoginCookie"] == null)
            {
                // Chuyển hướng đến trang đăng nhập
                return RedirectToPage("/Account/Login");
            }
            else
            {
                if (id == null)
                {
                    return NotFound();
                }

                Order = _context.Orders.Include(o => o.User).FirstOrDefault(o => o.OrderId == id);

                if (Order == null)
                {
                    return NotFound();
                }

                OrderDetails = _context.OrderDetails.Include(od => od.Product).Where(od => od.OrderId == id);

                return Page();
            }
        }
    }
}
