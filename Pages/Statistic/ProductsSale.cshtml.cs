﻿using FinalProjectPRN221.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace FinalProjectPRN221.Pages.Statistic
{
    public class ProductsSaleModel : PageModel
    {
        private readonly Project_PRN221Context _context;
        private readonly IConfiguration Configuration;


        public ProductsSaleModel(Project_PRN221Context context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;

        }

        // Thêm một thuộc tính public để lưu danh sách kết quả
        public List<Product> ProductSalesInfo { get; set; }
        public Product BestSellingProduct { get; set; }
        public Product LeastSellingProduct { get; set; }
        public PaginatedList<Product> Products { get; set; }
        public async Task<IActionResult> OnGet(int? pageIndex)
        {
            if (HttpContext.Request.Cookies["LoginCookie"] == null)
            {
                // Chuyển hướng đến trang đăng nhập
                return RedirectToPage("/Account/Login");
            }
            else
            {
                var productSales = _context.OrderDetails
                    .GroupBy(od => od.ProductId)
                    .Select(group => new
                    {
                        ProductId = group.Key,
                        TotalQuantity = group.Sum(od => od.Quantity)
                    })
                    .ToList();

                // Lấy thông tin về sản phẩm từ CSDL
                var products = _context.Products.ToList();

                // Tạo danh sách kết quả cuối cùng với thông tin sản phẩm và số lượng bán
                ProductSalesInfo = productSales
                    .Select(ps => new Product
                    {
                        ProductId = ps.ProductId.GetValueOrDefault(),
                        ProductName = products.FirstOrDefault(p => p.ProductId == ps.ProductId)?.ProductName,
                        StockQuantity = ps.TotalQuantity
                    })
                    .ToList();

                BestSellingProduct = ProductSalesInfo.OrderByDescending(p => p.StockQuantity).FirstOrDefault();
                LeastSellingProduct = ProductSalesInfo.OrderBy(p => p.StockQuantity).FirstOrDefault();

                // Lấy thông tin bán hàng theo sản phẩm
                var query = _context.Products.AsQueryable(); // Change this line to query products

                if (ProductSalesInfo != null)
                {
                    var pageSize = Configuration.GetValue("PageSize", 5);
                    Products = await PaginatedList<Product>.CreateAsync(query, pageIndex ?? 1, pageSize);
                }

                return Page();
            }
        }
    }
}
