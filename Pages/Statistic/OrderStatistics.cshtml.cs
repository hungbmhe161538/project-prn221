﻿using FinalProjectPRN221.Models;
using FinalProjectPRN221.Pages.Products;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FinalProjectPRN221.Pages.Statistic
{
    public class OrderStatisticsModel : PageModel
    {
        private readonly Project_PRN221Context _context;
        private readonly IConfiguration Configuration;

        public OrderStatisticsModel(Project_PRN221Context context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }

        public int TotalOrders { get; set; }
        public int? TotalRevenue { get; set; }

        public Order LowestOrder { get; set; }
        public Order HighestOrder { get; set; }
        public PaginatedList<Order> OrderStatistic { get; set; }

        public List<Order> Orders { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Salesman { get; set; }
        public string Status { get; set; }
        public async Task<IActionResult> OnGet(DateTime? startDate, DateTime? endDate, string salesman, string status, int? pageIndex)
        {
            if (HttpContext.Request.Cookies["LoginCookie"] == null)
            {
                // Chuyển hướng đến trang đăng nhập
                return RedirectToPage("/Account/Login");
            }
            else
            {
                StartDate = startDate;
                EndDate = endDate;
                Salesman = salesman;
                Status = status;
                // Lấy thông tin bán hàng theo sản phẩm
                IQueryable<Order> query = _context.Orders.Include(o => o.User);

                // Áp dụng bộ lọc theo ngày nếu được cung cấp
                if (startDate.HasValue)
                {
                    query = query.Where(o => o.OrderDate >= startDate);
                }

                if (endDate.HasValue)
                {
                    query = query.Where(o => o.OrderDate <= endDate);
                }

                // Áp dụng bộ lọc theo salesman
                if (!string.IsNullOrEmpty(salesman))
                {
                    query = query.Where(o => o.User.Username == salesman);
                }

                // Áp dụng bộ lọc theo status
                if (!string.IsNullOrEmpty(status))
                {
                    bool isPaid = (status.ToLower() == "paid");
                    query = query.Where(o => o.IsPaid == (isPaid ? "1" : "0"));
                }

                // Sắp xếp giảm dần theo OrderId
                query = query.OrderByDescending(o => o.OrderId);

                // Lấy tổng số đơn hàng sau khi áp dụng bộ lọc
                TotalOrders = query.Count();

                // Lấy tổng doanh thu sau khi áp dụng bộ lọc
                TotalRevenue = query.Sum(o => o.TotalAmount);

                // Lấy order có giá trị cao nhất
                HighestOrder = query.OrderByDescending(o => o.TotalAmount).FirstOrDefault();

                // Lấy order có giá trị thấp nhất
                LowestOrder = query.OrderBy(o => o.TotalAmount).FirstOrDefault();

                var pageSize = Configuration.GetValue("PageSize", 5);
                OrderStatistic = await PaginatedList<Order>.CreateAsync(query.AsNoTracking(), pageIndex ?? 1, pageSize);

                return Page();
            }
        }
    }
}
