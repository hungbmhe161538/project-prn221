﻿using FinalProjectPRN221.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace FinalProjectPRN221
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();
            builder.Services.AddDbContext<Project_PRN221Context>(options =>
            options.UseSqlServer(builder.Configuration.GetConnectionString("MyContr")));
            builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.Cookie.Name = "LoginCookie";
                    options.LoginPath = "/Account/Login"; // Đường dẫn tới trang đăng nhập
                    options.LogoutPath = "/Account/Logout"; // Đường dẫn tới trang đăng xuất
                    options.AccessDeniedPath = "/Account/AccessDenied"; // Đường dẫn tới trang truy cập bị từ chối
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(30); // Thời gian tồn tại của cookie
                });
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseStaticFiles();   

            app.UseRouting();
            var supportedCultures = new[] { new CultureInfo("vi-VN") }; // Culture của tiếng Việt
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("vi-VN"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });
            app.UseAuthorization();
            app.UseAuthentication();
            app.MapRazorPages();

            app.Run();
        }
    }
}